import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import PasungLogin from './component/PasungLogin';
import PasungIndex from './page/PasungIndex';
import PasungLapor from './component/PasungLapor';
import PasungSudahLapor from './component/PasungSudahLapor';
import PasungAktivasiAkun from './component/PasungAktivasiAkun';
import PasungProfilDokter from './component/PasungDokter/PasungProfilDokter';
import PasungProfilDokterUnpick from './component/PasungDokter/PasungProfilDokterUnpick'
import PasungRegisterPelapor from './component/PasungRegisterPelapor';
import PasungRegisterDokter from './component/PasungRegisterDokter';
import PasungDaftarDokter from './component/PasungDaftarDokter';
import PasungLoginDokter from './component/PasungDokter/PasungLoginDokter';
import PasungProfilPelapor from './component/PasungUser/PasungProfilPelapor';

class Router extends Component {
  render() {
    return (
     <div>
       <BrowserRouter>
                <div>
                    <Route exact path='/' component={PasungIndex} />

                    <Route exact path='/dokter' component={PasungIndex} />
                    <Route exact path='/pelapor' component={PasungIndex} />

                    <Route path='/login' component={PasungLogin} />
                    <Route path='/logindokter' component={PasungLoginDokter} />

                    <Route path='/doktor/:userId' component={PasungProfilDokter} />
                    <Route path='/pelapor/:userId' component={PasungProfilPelapor} />

                    <Route path='/registerpelapor' component={PasungRegisterPelapor} />
                    <Route path='/registerdokter' component={PasungRegisterDokter} />

                    <Route path='/lapor' component={PasungLapor} />

                    <Route path='/terimakasih' component={PasungSudahLapor} />
                    <Route path='/aktivasiakun' component={PasungAktivasiAkun} />
                    <Route path='/aktivasidokter' component={PasungDaftarDokter} />

                    <Route path='/unpick' component={PasungProfilDokterUnpick} />

                    
                </div>
            </BrowserRouter>


     </div>
    );
  }
}

export default Router;

import React from 'react';
import {Button} from 'reactstrap';
import {Row} from 'reactstrap';
import {Link} from 'react-router-dom';
import PasungNavbarTwo from './PasungNavbarTwo';

class DaftarDokter extends React.Component {
    render () {
        return (
            <div>
                <PasungNavbarTwo />
            <Row>
            <div className='BgDaftarDokter'>
                <h1 className='TextSatuDaftarDokter'>Tertarik Untuk Mensukseskan Indonesia Bebas Pasung?</h1>
                <p className='TextDuaDaftarDokter'>Jika Anda seorang dokter jiwa/ psikiater yang berdomisili di <strong>Jabodetabek</strong> dan <strong>tertarik</strong> untuk bergabung,</p> 
                <p className='TextTigaDaftarDokter'>kirimkan CV beserta STR (Post Internship) dan SIP aktif melalui email: <strong><a className='BtnKlik' href='#'>dokter@stoppasung.com</a> .</strong></p>
                <p className='TextEmpatDaftarDokter'>Untuk informasi dan pertanyaan, <strong><a className='BtnKlik' href='#'>klik di sini</a></strong> untuk menghubungi kami.</p>
                
            <div className='BotDaftarDokter'>
                <Link to='/'><Button className='BtnDaftarDokter'>Kembali Ke Halaman Utama</Button></Link>
            </div>
            </div>
            </Row>
            </div>
        );
    }
}

export default DaftarDokter;
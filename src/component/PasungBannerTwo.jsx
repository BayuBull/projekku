import React, {Component} from 'react';
import{Row} from 'reactstrap';
import '../assets/style/PasungStyle.scss';
import {Button} from 'reactstrap';
import {Link} from 'react-router-dom';


class PasungBannerTwo extends Component{
          render() {
            return (
              <div>
                    <Row >
                      <div className='BackgroundBot'>
                          <h2 className='TextLineStyle'>Anda Bisa Membantu Mereka Untuk Hidup Lebih Baik</h2>
                          <Link to='/lapor'><Button className='BtnBot'>Lapor Sekarang</Button></Link>
                        </div>
                    </Row>
             
                
              </div>
            );
     }
}

export default PasungBannerTwo;

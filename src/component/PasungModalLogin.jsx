import React, {Component} from 'react';
import {
   Row,Col,
    Modal,ModalBody,ModalHeader,Button} from 'reactstrap';
import'../assets/style/PasungStyle.scss';
import {Link} from 'react-router-dom';




class PasungModalLogin extends Component{
    constructor(props) {
        super(props);
        this.state = {

            modal: false,

        }
        this.toggle = this.toggle.bind(this);

    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }
    render(){
        return(
            <div>
                <Row>
                        <Col>
                        <div>
                            <p className='register' onClick={this.toggle}>Login</p>
                            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                            <ModalHeader toggle={this.toggle}>Hallo, Siapa Anda ?</ModalHeader>
                            <ModalBody>
                            <div className='PasungBodyModal'>
                             <Link to='/logindokter' ><Button className='ModalBtnOne' color="danger">Dokter</Button></Link>
                             <Link to='/login' ><Button className='ModalBtnOne' color="danger">Pelapor</Button></Link>
                            
                            </div>
                           
                            </ModalBody>
                            </Modal>
                        </div> 
                        </Col>
                    </Row>
                </div>
        )
    }
}

export default PasungModalLogin ;
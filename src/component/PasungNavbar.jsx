import React, {Component} from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    Nav,
    NavItem,
    NavLink,
    } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Link,Redirect} from 'react-router-dom';
import '../assets/style/_PasungNavbar.scss';
import logo from '../assets/Icon/logo.png';
import PasungModal from '../component/PasungModal';
import loggedIn from '../helper/is_logged_in';
import store from 'store';



class PasungNavbar extends Component{
    constructor(props) {
      super(props);
      this.handleLogout = this.handleLogout.bind(this);

  
      this.toggle = this.toggle.bind(this);
      this.state = {
        isOpen: false
      };
    }
    toggle() {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    handleLogout = (e) => {
      store.remove('loggedIn');
      return <Redirect to='/login' /> 

  }
  render() {
    console.log('ini login', loggedIn())

    let sudahlogin;
    if(loggedIn()) {
      sudahlogin =  <NavLink > <button onClick={this.handleLogout}>Logout</button></NavLink>
    } else {
      sudahlogin =  <NavLink ><Link  className='NavLink' to='/login'>Login</Link></NavLink>
    }

    let registerin;
    if(loggedIn()) {
      registerin = <NavLink><Link to='/profiledokter'>Profile</Link></NavLink>
    } else {
      registerin = <NavLink ><PasungModal /></NavLink>
    }
    console.log('nama',this.props)
    return (
      <div>
        <div className='Nav'>
        <Navbar dark expand="md">
        <Link to='/'><img className='logoone' src={logo} alt='Pasung.com'></img></Link>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                {sudahlogin}
              </NavItem>
              <NavItem>
                {registerin}
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
        </div>
      </div>
    );
  }
}

export default PasungNavbar;

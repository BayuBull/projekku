import React from 'react';
import '../assets/style/PasungStyle.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col, Breadcrumb, BreadcrumbItem} from 'reactstrap';

export default class PasungUserLaporanDetail extends React.Component{
    render(){
        return(
            <div>
                <Container>
                    <Row>
                        <Col md='8'>
                        <div>
                            <Breadcrumb tag="nav" listTag="div">
                                <BreadcrumbItem tag="a" href="#">Home</BreadcrumbItem>
                                <BreadcrumbItem tag="a" href="#">Laporan Anda</BreadcrumbItem>
                                <BreadcrumbItem active tag="a" href="#">Nama Pasien</BreadcrumbItem>
                            </Breadcrumb>
                        </div>
                            <div className='FontPasien'>
                            Laporan Anda
                            </div>
                            <div className='line1'></div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
import React from 'react';
import '../assets/style/PasungStyle.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col, Breadcrumb, BreadcrumbItem, Label, Input} from 'reactstrap';

export default class PasungUserBuatLaporan extends React.Component {
    render () {
        return(
            <div>
                <Container>
                    <Row>
                        <Col md='8'>
                            <div>
                            <Breadcrumb tag="nav" listTag="div">
                                <BreadcrumbItem tag="a" href="#">Home</BreadcrumbItem>
                                <BreadcrumbItem active tag="a" href="#">Laporan Anda</BreadcrumbItem>
                            </Breadcrumb>
                            </div>
                            <div className='FontPasien'>
                            Laporan Anda
                            </div>
                            <div className='line1'></div>
                            <div className='KotakUserBuatLaporan'>
                                <div className='FotoUserLaporan'>
                                    <img className='FotoUserLaporan1' src="https://asset.kompas.com/data/photo/2017/01/14/0857430Pasung-1780x390.jpg" alt="FotoUserLaporan1"/>
                                </div>
                                <div className='NamaUserBuatLaporan'>
                                    <h4 className='NamaUserBuatLaporan1'>
                                        Nama Pasien, xx Th
                                    </h4>
                                    <h5 className='NamaUserBuatLaporan2'>
                                        Kota
                                    </h5>
                                </div>
                            </div>
                            <div className='ButtonUserBuatLaporan'>
                            <Label for="exampleFile">Buat Laporan Baru</Label>
                            <Input type="file" name="file" id="exampleFile" />
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
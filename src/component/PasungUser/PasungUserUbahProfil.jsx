import React from 'react';
import '../assets/style/PasungStyle.scss';
import {Container, Row, Col, Breadcrumb, BreadcrumbItem, Button, Form, FormGroup, Label, Input, FormText} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';


export default class PasungUserUbahProfil extends React.Component {
    render() {
        return(
            <div>
                <Container>
                    <Row>
                        <Col md='8'>
                        <div>
                        <Breadcrumb tag="nav" listTag="div">
                            <BreadcrumbItem tag="a" href="#">Home</BreadcrumbItem>
                            <BreadcrumbItem  tag="a" href="#">Akun Saya</BreadcrumbItem>
                            <BreadcrumbItem active tag="a" href="#">Ubah Profile</BreadcrumbItem>
                        </Breadcrumb>
                        </div>
                        <div className='FontPasien'>
                            Ubah Profile
                          </div>
                        <div className='line1'></div>
                        <div className='PasungUserUbahProfil'>
                        <Form>
                            <FormGroup  row>
                            <Label for="exampleEmail" sm='4'>Nama Lengkap</Label>
                            <Col sm='8'>
                                <Input type="email" name="email" id="exampleEmail" placeholder="with a placeholder" />
                            </Col>
                            </FormGroup>
                            <FormGroup row>
                            <Label for="examplePassword" sm={4}>Email</Label>
                            <Col sm={8}>
                                <Input type="password" name="password" id="examplePassword" placeholder="password placeholder" />
                            </Col>
                            </FormGroup>
                            <FormGroup row>
                            <Label for="exampleSelect" sm={4}>Tempat Tinggal</Label>
                            <Col sm={8}>
                                <Input type="select" name="select" id="exampleSelect" />
                            </Col>
                            </FormGroup>
                            <FormGroup row>
                            <Label for="exampleFile" sm={4}>Foto Profil</Label>
                            <Col sm={8}>
                                <Input type="file" name="file" id="exampleFile" />
                                <FormText color="muted">
                                    Foto yang diupload disarankan 
                                    berukuran 80px x 80px dan memiliki 
                                    format JPEG atau PNG.
                                </FormText>
                            </Col>
                            </FormGroup>
                            <FormGroup row>
                            <Label for="exampleText" sm={4}>Bio Singkat</Label>
                            <Col sm={8}>
                                <Input type="textarea" name="text" id="exampleText" />
                            </Col>
                            </FormGroup>
                            <div>
                            <FormGroup check row>
                            <Col sm={{ size: 10, offset: 4 }}>
                                <Button style={{backgroundColor:'#de3163', fontFamily: 'Arial', width:'225px', borderRadius:'8x', height:'50px'}}>Simpan</Button>
                            </Col>
                            </FormGroup>
                            </div>
                        </Form>    
                        </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
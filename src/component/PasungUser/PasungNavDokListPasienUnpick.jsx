import React from 'react';
import '../../assets/style/PasungStyle.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Button, Breadcrumb, BreadcrumbItem} from 'reactstrap';
import axios from 'axios';



export default class PasungNavUserListPasienUnpick extends React.Component{
    constructor(){
        super();
        this.state ={
           Lapor:[]
        }
    }
    componentDidMount(){
        axios.get(`https://stoppasung.herokuapp.com/laporan/belumditangani`)
        .then(respon=>{
            this.setState({
                Lapor:respon.data
                
            })
        })
        
    }
    render() {
        return(
            <div>
                <Container>
                    <Row>
                        <div className='BackPfofilDoxUnpick'>
             
                            <Breadcrumb tag="nav" listTag="div">
                                <BreadcrumbItem tag="a" href="#">Home</BreadcrumbItem>
                                <BreadcrumbItem  tag="a" href="#">Pasien Anda</BreadcrumbItem>
                                <BreadcrumbItem active tag="a" href="#">Unpicked</BreadcrumbItem>
                            </Breadcrumb>
                            </div>
                            <div>
                            <div className='BackgorundBan'> 
                                {
                                
                                    this.state.Lapor.map((Lapor,index) =>
                                    
                                   
                                    <Row>
                                    <div className ='CardUserUnpick' >
                                    <div className ='BackContUserUnpick'  key={index} >
                                        <img className='PhotoUserUnpick' src={Lapor.photoPasien}></img>
                                        <h3 className='UserTitleUnpick'>{Lapor.namaPasien}</h3>
                                        <p className='UserDeskripsiUnpik'>{Lapor.kota}</p>
                                        <Button className='BtnDetUserUnpick'>Detail Pasien</Button>
                                        </div>
                                        </div>
                                    </Row>
                                   
                            
                            
                                    )
                                }
                                     </div>
                        </div>
                            
                    </Row>
                </Container>
            </div>
        );
    }
}

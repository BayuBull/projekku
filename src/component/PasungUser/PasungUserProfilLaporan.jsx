import React from 'react';
import '../assets/style/PasungStyle.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col, Breadcrumb, BreadcrumbItem, Button} from 'reactstrap';
import logo4 from '../assets/img/ImgDokProfil.png';

export default class PasungUserProfilLaporan extends React.Component {
    render() {
        return(
            <div>
                <Container>
                    <Row>
                        <Col md='8'>
                        <div>
                            <Breadcrumb tag="nav" listTag="div">
                                <BreadcrumbItem tag="a" href="#">Home</BreadcrumbItem>
                                <BreadcrumbItem tag="a" href="#">Laporan Anda</BreadcrumbItem>
                                <BreadcrumbItem active tag="a" href="#">Nama Pasien</BreadcrumbItem>
                            </Breadcrumb>
                        </div>
                            <div className='FontPasien'>
                            Laporan Anda
                            </div>
                            <div className='line1'></div>
                            <div className='KotakDokProfilLaporan'>
                                <div className='NamaPasienDokProfil'>
                                <h3 className='NamaPasienDokProfil1'>  
                                Nama Pasien
                                <h4 className='NamaPasienDokProfil2'>
                                Gender, Usia
                                </h4>
                                 </h3>
                                </div>
                            <div className='FotoDokProfil'>
                                <img className='FotoDokProfil1' src="https://asset.kompas.com/data/photo/2017/01/14/0857430Pasung-1780x390.jpg" alt="FotoDokProfil"/>
                            </div>
                            <div className='ButtonDokProfil'>
                            <Button outline color="primary">Laporan</Button>{' '}
                            <Button outline color="primary" style={{marginLeft:'70px'}}>Tentang Pasien</Button>{' '}
                            </div>
                        <div className='KotakDokProfil1'>
                        <img src={logo4} alt="logo4"/>
                        </div>
                        <div className='KotakDokProfil2'>
                        <img src={logo4} alt="logo4"/>
                        </div>
                        </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
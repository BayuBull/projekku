import React from 'react';
import {Button} from 'reactstrap';
import {Row} from 'reactstrap';
import {Link} from 'react-router-dom';
import PasungNavbarTwo from './PasungNavbarTwo';

class AktivasiAkun extends React.Component {
    render () {
        return (
            <div>
                <PasungNavbarTwo />
            <Row>
            <div className='BgAktivasiAkun'>
                    <h1 className='TextSatuAktivasiAkun'>Verifikasi Email Anda</h1>
                    <div className='BoxTextAktivasiAkun'>
                    <p className='TextDuaAktivasiAkun'>Cek email Anda dan klik aktivasi akun Pasung.com</p>
                    </div>

                <div className='BotAktivasiAkun'>
                <Link to='/'><Button className='BtnTrmksh'>Kembali Ke Halaman Utama</Button></Link>
                </div>
            </div>
            </Row>
            </div>
        );
    }
}

export default AktivasiAkun;
import React, {Component} from 'react';
import {Container, Row} from 'reactstrap';
import BannerTahapSatu from '../assets/Img/Home/tahap1.svg';
import BannerTahapDua from '../assets/Img/Home/tahap2.svg';
import BannerTahapTiga from '../assets/Img/Home/tahap3.svg';

class PasungBannerTahap extends Component {
    render () {
        return (
            <div>
                <Container>
                    <Row>
                        <div className='BgBannerTahapSatu'>
                            <div className='TextBannerTahapSatu'>
                                <div className='TextTahapSatu'>
                                    <h1 className='TextSatuTahapSatu'>Buat Laporan</h1>
                                    <p className='TextDuaTahapSatu'>Jika kalian melihat orang di sekitar kalian yang masih dibelenggu, laporkan mereka dengan cara  klik tombol "Lapor Sekarang" di halaman ini dan isilah data-data terkait pasien.</p>
                                </div>
                            </div>

                           
                            <div className='ImgBannerTahapSatu'>
                                <img className='BannerTahapSatu' src={BannerTahapSatu} alt='BannerTahapSatu'></img>
                            </div>
                            </div>
                            
                        




                            <div className='BgBannerTahapDua'>
                            <div className='ImgBannerTahapDua'>
                                <img className='BannerTahapDua' src={BannerTahapDua} alt='BannerTahapDua'></img>
                            </div>

                            <div className='TextBannerTahapDua'>
                                <div className='TextTahapDua'>
                                    <h1 className='TextSatuTahapDua'>Data Diterima</h1>
                                    <p className='TextDuaTahapDua'>Jangan khawatir informasi yang kamu berikan tidak direspon. Tim kami akan memberikan informasi pasien langsung kepada dokter terdekat.</p>
                                </div>
                            </div>
                            </div>

                            





                            <div className='BgBannerTahapTiga'>
                            <div className='TextBannerTahapTiga'>
                                <div className='TextTahapTiga'>
                                    <h1 className='TextSatuTahapTiga'>Penanganan Dokter</h1>
                                    <p className='TextDuaTahapTiga'>Dokter akan menanggapi laporan kalian dengan cepat. Setelah itu, dokter akan memberikan treatment secara online dan pengobatan akan dilakukan sampai pasien sembuh.</p>
                                </div>
                            </div>

                           
                            <div className='ImgBannerTahapTiga'>
                                <img className='BannerTahapTiga' src={BannerTahapTiga} alt='BannerTahapTiga'></img>
                            </div>
                            </div>
                        
                    </Row>
                </Container>
            </div>
        );
    }
}

export default PasungBannerTahap;
import React from 'react';
import {Button} from 'reactstrap';
import {Container} from 'reactstrap';
import {Link} from 'react-router-dom';
import PasungNavbarTwo from './PasungNavbarTwo';


class AkunActivated extends React.Component {
    render () {
        return (
            <div>
                <PasungNavbarTwo />
            <div className='BgAkunActivated'>
                <Container className='ContainerAkunActivated'>
                <div>
                    <h1 className='TextSatuAkunActivated'>Aktivasi Sukses!</h1>
                    <p className='TextDuaAkunActivated'>Silahkan login dengan alamat email <strong>ikbalmaulana@gmail.com</strong> dan password yang telah Anda tentukan</p>
                </div>
                </Container>

                <div className='BotActivated'>
                <Link to='/'><Button className='BtnActivated'>Login</Button></Link>
                </div>
            </div>
            </div>
        );
    }
}

export default AkunActivated;
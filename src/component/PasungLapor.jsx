import React, {Component} from 'react';
import {Container,Row,Col} from 'reactstrap';
import '../assets/style/PasungStyle.scss';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import PasungNavbarTwo from './PasungNavbarTwo';
import axios from 'axios';


class PasungLapor extends Component {
constructor(props){
    super(props);
    this.state={
      alamatPasien :'',
      emailDokter :'',
      emailPelapor : '',
      jenisKelamin : '',
      kondisiPasien : '',
      kota : '',
      namaPasien :'',
      photoPasien : '',
      usia : '',
    }
}

//methot Post
handlealamatPasienChange = event => {this.setState({alamatPasien:event.target.value})};
handleemailDokterChange = event => {this.setState({emailDokter:event.target.value})};
handleemailpelaporChange = event => {this.setState({emailpelapor:event.target.value})};
handlejeisKelaminChange = event =>{this.setState({jenisKelamin:event.target.value})};
handlekondisiPasienChange = event => {this.setState({kondisiPasien:event.target.value})};
handlekotaChange = event =>{this.setState({kota:event.target.value})};
handlenamaPasienChange = event => {this.setState({namaPasien:event.target.value})};
handlephotoPasienChange =event => {this.setState({photoPasien:event.target.value})};
handleusiaChange = event => {this.setState({usia:event.target.value})}

handleSudmit = event =>{
    event.preventDefault();

    axios.post(`https://stoppasung.herokuapp.com/laporan`,
    {
        alamatPasien:this.state.alamatPasien,
        emailDokter:this.state.emailDokter,
        emailPelapor:this.state.emailPelapor,
        jenisKelamin: this.state.jenisKelamin,
        kota:this.state.kota,
        namaPasien:this.state.namaPasien,
        photoPasien:this.state.photoPasien,
        kondisiPasien:this.state.kondisiPasien
    }
    )
.then (respon=>{
        console.log(respon);
        console.log(respon.data);

        this.props.history.push('/terimakasih')
})
}

    render(){
        console.log("masuk",this.state.namaPasien);
        return(
            <div>
                
                <PasungNavbarTwo />
                <Container>
                    <Row>
                        <div className='PasungLaporBack'>
                            <Col sm='6'>
                            <div className='FormUploadImg'>
                            <Form onSubmit={this.handleSudmit}>
                                <FormGroup>
                                <Label for="exampleFile">Image</Label>
                                <Input type="file" name={this.state.photoPasien} onChange={this.handlephotoPasienChange} id="exampleFile" />
                                <FormText color="muted">
                                    Masukan Picture Atau Image .
                                </FormText>
                                </FormGroup>
                            </Form>
                            </div> 
                            </Col>
                            <Col sm='6'>
                            <div className='FormImputLaporan'>
                            <Form onSubmit={this.handleSudmit}>
                                        <FormGroup>
                                        <Label for="NamaID">Nama Pasien</Label>
                                        <Input type="text" name={this.state.namaPasien} onChange={this.handlenamaPasienChange} id="NamaID"  />
                                        </FormGroup>
                                      
                                        <div className='JenisKel'>
                                        <FormGroup>
                                        <Label for="jK">Jenis Kelamin</Label>
                                        <Input type="select"className='JenisKelamin' name={this.state.jenisKelamin} onChange={this.handlejeisKelaminChange} id="jK">
                                            <option value='laki-laki'>Laki-Laki</option>
                                            <option value='perempuan'>Perempuan</option>
                                        </Input>
                                        <Label for="UsiaId">Usia</Label>
                                        <Input type="text" name={this.state.usia} onChange={this.handleusiaChange} className='Usia' id="UsiaId"  />
                                        </FormGroup>
                                        </div>
                                        <FormGroup>
                                        <Label for="KotaId">Kota</Label>
                                        <Input type="text" name={this.state.kota} onChange={this.handlekotaChange} id="AlamatId" placeholder="Contoh = JakartaPasat" />
                                        </FormGroup>
                                        <FormGroup>
                                        <Label for="AlamatId">Alamat</Label>
                                        <Input type="text" name={this.state.alamatPasien} onChange={this.handlealamatPasienChange} id="AlamatId" />
                                        </FormGroup>
                                        
                                        <FormGroup>
                                        <Label for="KondisiId">Masukan Kondisi Pasien</Label>
                                        <Input type="text" name={this.state.kondisiPasien} onChange={this.kondisiPasien} className='Kondisi' id="KondisiId" />
                                        </FormGroup>
                                        <Button type='Submit' className='BtnLapor2'>Lapor</Button>                             
                            </Form>
                            </div>
                            </Col>
                        </div>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default PasungLapor;
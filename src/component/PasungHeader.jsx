import React from 'react';
import { Button } from 'reactstrap';
import '../assets/style/PasungStyle.scss';
import PasungNavbar from './PasungNavbar';
import {Row} from 'reactstrap';
import {Link} from 'react-router-dom';


export default class PasungHeader extends React.Component {
    
    render() {
        
        return(
            
            <div>
                <div className='BackgroundHeader'>
                <PasungNavbar />
                    <div className='ContainerHeader'>
                    <div className='PasungTaglineHome'>
                    <p>Laporkan Kasus Pemasungan di Sekitar Anda</p>
                    </div>
                    <div className="ContainerBotHome">
                    <Row>
                    <Link  className='BtnLapor' to='/lapor'><Button  className='Lapor'>Lapor Sekarang</Button></Link>
                    <Link  className='BtnDocter' to='/aktivasidokter'><Button outline color className='Docter'>Saya Dokter</Button></Link>
                    </Row>
                    </div>
                    </div>
                    
                </div>
            </div>
            
        );
    }
}
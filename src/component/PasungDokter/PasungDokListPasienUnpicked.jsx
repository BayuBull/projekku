import React from 'react';
import '../assets/style/PasungStyle.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col, Button, Breadcrumb, BreadcrumbItem, Card, CardBody, CardImg, CardTitle, CardSubtitle} from 'reactstrap';

export default class PasungDokListPasienUnpicked extends React.Component{
    render() {
        return(
            <div>
                <Container>
                    <Row>
                        <Col md='8'>
                        <div>
                            <Breadcrumb style={{padding:'10px'}} tag="nav" listTag="div">
                                <BreadcrumbItem tag="a" href="#">Home</BreadcrumbItem>
                                <BreadcrumbItem  tag="a" href="#">Pasien Anda</BreadcrumbItem>
                                <BreadcrumbItem active tag="a" href="#">Unpicked</BreadcrumbItem>
                            </Breadcrumb>
                            </div>
                          <div className='FontPasien'>
                            Daftar Pasien
                          </div>
                        <div className='line'></div>
                        <div className='CardListPasienUnpicked'>
                        <Card >
                            <CardImg  top width="100%" src="https://asset.kompas.com/data/photo/2017/01/14/0857430Pasung-1780x390.jpg" alt="Card image cap" />
                            <CardBody>
                            <CardTitle style={{fontFamily:'Arial', fontSize:'20px'}}>Dodi Irawan, 33th</CardTitle>
                            <CardSubtitle >Bekasi</CardSubtitle>
                            </CardBody>
                            <Button style={{backgroundColor:'#de3163'}} className='ButtonPasienUnpicked'>Tangani</Button>
                        </Card>
                        </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
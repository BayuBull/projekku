import React from 'react';
import '../assets/style/PasungStyle.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col, Breadcrumb, BreadcrumbItem, Button, Table} from 'reactstrap';

export default class PasungDokProfilTentangPasien extends React.Component {
    render() {
        return(
            <div>
                <Container>
                    <Row>
                        <Col md='8'>
                        <div>
                            <Breadcrumb style={{padding:'10px'}} tag="nav" listTag="div">
                                <BreadcrumbItem tag="a" href="#">Home</BreadcrumbItem>
                                <BreadcrumbItem  tag="a" href="#">Pasien Anda</BreadcrumbItem>
                                <BreadcrumbItem active tag="a" href="#">Picked</BreadcrumbItem>
                            </Breadcrumb>
                            </div>
                          <div className='FontPasien'>
                            Pasien Anda
                          </div>
                        <div className='line'></div>
                        <div className='KotakDokProfilTtgPasien'>
                        <div>
                        <div className='NamaPasienDokProfil'>
                                <h3 className='NamaPasienDokProfil1'>  
                                Nama Pasien
                                <h4 className='NamaPasienDokProfil2'>
                                Gender, Usia
                                </h4>
                                 </h3>
                                </div>
                            <div className='FotoDokProfil'>
                                <img className='FotoDokProfil1' src="https://asset.kompas.com/data/photo/2017/01/14/0857430Pasung-1780x390.jpg" alt="FotoDokProfil"/>
                            </div>
                            <div className='ButtonDokProfil'>
                            <Button outline color="primary">Laporan</Button>{' '}
                            <Button outline color="primary" style={{marginLeft:'70px'}}>Tentang Pasien</Button>{' '}
                            </div>
                        </div>
                        <div className='TableDokTtgPasien'>
                            <Table>
                            <tbody>
                            <tr>
                                <th scope="row">Nama Lengkap</th>
                                <td></td>
                                <td>Nama Pasien</td>
                                
                            </tr>
                            <tr>
                                <th scope="row">Jenis Kelamin</th>
                                <td></td>
                                <td>Gender Pasien</td>
                                
                            </tr>
                            <tr>
                                <th scope="row">Usia</th>
                                <td></td>
                                <td>Usia Pasien</td>
                                
                            </tr>
                            <tr>
                                <th scope="row">Alamat</th>
                                <td></td>
                                <td>Alamat Pasien</td>
                                
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td></td>
                                <td></td>
                                
                            </tr>
                            </tbody>
                            </Table>
                        </div>
                        <div className='LatarBelakangPasien'>
                            <h3>
                                Latar Belakang
                            </h3>
                        </div>
                        <div className='LatarBelakangPasien1'>
                            <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 

Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                        </div>
                        </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
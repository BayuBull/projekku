import React, {Component} from 'react';
import {Row,Container} from 'reactstrap';
import PasungCardProfilDokter from '../PasungDokter/PasungCardProfilDokter';
import PasungFooter from '../Pasung Footer';
import PasungNavDokListPasienUnpick from '../PasungDokter/PasungNavDokListPasienUnpick';





class PasungProfilDokterUnpick extends Component {
    render(){
        return(
                <div>
                    <Container>
                        <Row>
                              <div className='BackgroundProDok'>
                           <div className='ProfilDocBoxOne'>
                            <PasungCardProfilDokter />
                            </div>
                            <Row>
                            <div className='ProfilDocBoxTwo'>
                           
                                <PasungNavDokListPasienUnpick />
                               
                            </div>
                            </Row>
                            </div>
                        </Row>
                        
                    </Container>
                    <PasungFooter />
                </div>
        )
    }
}

export default PasungProfilDokterUnpick ;
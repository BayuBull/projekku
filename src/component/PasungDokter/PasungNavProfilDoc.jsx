import React from 'react';
import { Collapse, Navbar, Button, Nav, NavItem, NavLink } from 'reactstrap';

export default class PasungNavProfilDoc extends React.Component {
  constructor(props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }

  toggleNavbar() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }
  render() {
    return (
      <div>
          <Button className="BtnProfilDocPasien" color="info" onClick={this.toggleNavbar} >Pasien Saya</Button>
          <Collapse isOpen={!this.state.collapsed} navbar>
            <Nav navbar>
              <NavItem>
                <NavLink href="#">Picked</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#">Unpicked</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
      </div>
    );
  }
}
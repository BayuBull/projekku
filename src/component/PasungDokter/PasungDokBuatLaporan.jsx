import React from 'react';
import '../assets/style/PasungStyle.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col, Breadcrumb, BreadcrumbItem, Form, FormGroup, Input, Label, Button} from 'reactstrap';

export default class PasungDokLaporan extends React.Component {
    render (){
        return(
            <div>
                <Container>
                    <Row>
                        <Col md='8'>
                        <div>
                        <Breadcrumb style={{padding:'10px'}} tag="nav" listTag="div">
                            <BreadcrumbItem tag="a" href="#">Home</BreadcrumbItem>
                            <BreadcrumbItem  tag="a" href="#">Pasien Anda</BreadcrumbItem>
                            <BreadcrumbItem  tag="a" href="#">Picked</BreadcrumbItem>
                            <BreadcrumbItem active tag="a" href="#">Nama Pasien</BreadcrumbItem>
                        </Breadcrumb>
                        </div>
                        <div className='FontPasien'>
                            Buat Laporan
                          </div>
                        <div className='line'></div>
                        <div className='PasungDokBuatLaporan'>
                            <Form style={{padding:'30px'}}>
                            <FormGroup>
                                <Input type="email" name="email" id="exampleEmail" placeholder="Subject" />
                            </FormGroup>
                            <FormGroup>
                                <Input type="textarea" name="text" id="exampleText" />
                            </FormGroup>
                            <FormGroup>
                            <Label>Unggah Foto</Label>
                            <Input type="file" class="uploadFile"/>
                            </FormGroup>
                            <Button className='ButtonBuatLaporanDok' style={{backgroundColor:'#de3163'}}> Simpan</Button>
                            </Form>
                        </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}
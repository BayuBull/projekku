import React, {Component} from 'react';
import {Row,Col} from 'reactstrap';
import tes from '../../assets/Img/BackgroundHome.jpg';
import {Button} from 'reactstrap';
import PasungNavProfilDoc from '../PasungDokter/PasungNavProfilDoc';
import PasungNavProfilDocTwo from '../PasungDokter/PasungNavProfilDocTwo';




class PasungCardProfilDokter extends Component {
    constructor(props){
        super(props);

        
    }
   
    render(){
        console.log('card',this.props)
        return(
                <div>
                    <Row>
                        <div className='DokProfilCard'>
                                <div className='DokBackgroundProfilCard'>
                                            <img className='DokBoxProfilImg' src={tes} />
                                            <div className='DokContenProfil'>
                                            <h1 className='DokNamaProfil'>nama</h1>
                                        </div>
                                    </div>
                                    <div>
                                     
                                        <div className='DokBoxBtnProfil'>
                                        <Button className='BtnEditProfilDok'>Edit Dokter</Button>
                                        <div className='BoxNavProfilDOk'>
                                        <PasungNavProfilDoc />
                                        <PasungNavProfilDocTwo />
                                        </div>
                                       </div>
                                  
                                    </div>
                                </div>
                    </Row>
                </div>
        )
    }
}

export default PasungCardProfilDokter ;
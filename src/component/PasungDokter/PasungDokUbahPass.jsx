import React from 'react';
import '../assets/style/PasungStyle.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col, Breadcrumb, BreadcrumbItem, Button, Form, FormGroup, Label, Input,} from 'reactstrap';

export default class PasungDokUbahPass extends React.Component{
    render() {
        return(
            <div>
                <Container>
                    <Row>
                        <Col md='8'>
                            <div>
                            <Breadcrumb tag="nav" listTag="div">
                                <BreadcrumbItem tag="a" href="#">Home</BreadcrumbItem>
                                <BreadcrumbItem  tag="a" href="#">Akun Saya</BreadcrumbItem>
                                <BreadcrumbItem active tag="a" href="#">Ubah Password</BreadcrumbItem>
                            </Breadcrumb>
                            </div>
                            <div className='FontPasien'>
                            Ubah Password
                            </div>
                            <div className='line'></div>
                            <div className='PasungDokUbahPass'>
                            <Form>
                            <FormGroup row>
                            <Label for="exampleEmail" sm='4'>Password Lama</Label>
                            <Col >
                                <Input type="email" name="email" id="exampleEmail" placeholder="with a placeholder" />
                            </Col>
                            </FormGroup>
                            <FormGroup row>
                            <Label for="examplePassword" sm='4' >Password Baru</Label>
                            <Col >
                                <Input type="password" name="password" id="examplePassword" placeholder="password placeholder" />
                            </Col>
                            </FormGroup>
                            <FormGroup row>
                            <Label for="examplePassword" sm='4'>Konfirmasi Password Baru</Label>
                            <Col >
                                <Input type="password" name="password" id="examplePassword" placeholder="password placeholder" />
                            </Col>
                            </FormGroup>
                            <div>
                            <FormGroup check row>
                            <Col sm={{ size: 10, offset: 4 }}>
                                <Button style={{backgroundColor:'#de3163', fontFamily: 'Arial', width:'225px', borderRadius:'8x', height:'50px'}}>Simpan</Button>
                            </Col>
                            </FormGroup>
                            </div>
                            </Form>
                            </div>   
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
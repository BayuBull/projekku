import React from 'react';
import '../assets/style/PasungStyle.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col, Breadcrumb, BreadcrumbItem, Button,  Jumbotron} from 'reactstrap';

export default class PasungDoklaporanDetail extends React.Component{
    render() {
        return(
            <div>
                <Container>
                    <Row>
                        <Col md='8'>
                        <div>
                            <Breadcrumb style={{padding:'10px'}} tag="nav" listTag="div">
                                <BreadcrumbItem tag="a" href="#">Home</BreadcrumbItem>
                                <BreadcrumbItem  tag="a" href="#">Pasien Anda</BreadcrumbItem>
                                <BreadcrumbItem  tag="a" href="#">Picked</BreadcrumbItem>
                                <BreadcrumbItem active tag="a" href="#">Nama Pasien</BreadcrumbItem>
                            </Breadcrumb>
                            </div>
                          <div className='FontPasien'>
                            <Button onClick="goBack" style={{margin:'15px'}}>Back</Button>                          
                            Tim StopPasung                
                          </div>
                        <div className='line'></div>
                        <div className='TextPasungDokLaporanDetail'>
                            <Jumbotron  style={{height:'200px', marginTop:'20px', }}>
                            <div className='FontPasien' style={{textAlign:'start'}} >
                            Hallo, Dokter ...
                            </div>
                            <div className='lineLaporanDetail'></div>
                            <div className='TextDokDetail2'>Laporan anda mengenai pasien Dudi Setiudi, telah kami kirimkan kepada pelapor Ikbal Maulana.</div>
                            </Jumbotron>
                        </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
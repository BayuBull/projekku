import React from 'react';
import '../../assets/style/PasungStyle.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Button, Breadcrumb, BreadcrumbItem} from 'reactstrap';
import axios from 'axios';



export default class PasungNavDokListPasienUnpick extends React.Component{
    constructor(){
        super();
        this.state ={
           Lapor:[]
        }
    }
    componentDidMount(){
        axios.get(`https://stoppasung.herokuapp.com/laporan/belumditangani`)
        .then(respon=>{
            this.setState({
                Lapor:respon.data
                
            })
        })
        
    }
    render() {
        return(
            <div>
                <Container>
                    <Row>
                        <div className='BackPfofilDoxUnpick'>
             
                            <Breadcrumb tag="nav" listTag="div">
                                <BreadcrumbItem tag="a" href="#">Home</BreadcrumbItem>
                                <BreadcrumbItem  tag="a" href="#">Pasien Anda</BreadcrumbItem>
                                <BreadcrumbItem active tag="a" href="#">Unpicked</BreadcrumbItem>
                            </Breadcrumb>
                            </div>
                            <div>
                            <div className='BackgorundBan'> 
                                {
                                
                                    this.state.Lapor.map((Lapor,index) =>
                                    
                                   
                                    <Row>
                                    <div className ='CardDocUnpick' >
                                    <div className ='BackContDocUnpick'  key={index} >
                                        <img className='PhotoDokUnpick' src={Lapor.photoPasien}></img>
                                        <h3 className='DocTitleUnpick'>{Lapor.namaPasien}</h3>
                                        <p className='DokDeskripsiUnpik'>{Lapor.kota}</p>
                                        <Button className='BtnDetDocUnpick'>Detail Pasien</Button>
                                        </div>
                                        </div>
                                    </Row>
                                   
                            
                            
                                    )
                                }
                                     </div>
                        </div>
                            
                    </Row>
                </Container>
            </div>
        );
    }
}

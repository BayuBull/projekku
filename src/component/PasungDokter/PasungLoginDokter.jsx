import React, {Component} from 'react';
import PasungNavbarTwo from '../PasungNavbarTwo';
import {Container,Row} from 'reactstrap';
import { Button, Form, FormGroup, Label, Input, } from 'reactstrap';
import '../../assets/style/PasungStyle.scss';
import IconFb from '../../assets/Icon/iconfb.svg';
import IconGmail from '../../assets/Icon/icongmail.svg';
import loggedIn from '../../helper/is_logged_in';
import store from 'store'
import { Link, Redirect } from 'react-router-dom'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import axios from 'axios'




class PasungLoginDokter extends Component{
    constructor(props) {
        super(props)

        this.state = {
            email: "",
            password: "",
            error: false,
            userid: ''
           
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    
    }

    handleChange(e) {
        const target = e.target
        const value = target.value
        const name = target.name

        this.setState({
            [name]: value
        })
    }

    handleSubmit(e) {
        e.preventDefault()
        this.setState( {error: false} )
        const user = {
            email: this.state.email,
            password:  this.state.password
        }
        // alert(user.username)
        
        axios.post('https://stoppasung.herokuapp.com/user/login', user)
        .then( res => {
            console.log(res.data);
            this.setState({userid: res.data.idUser})
            this.setState({error: false})
            if(res.data.userStatus === 'ACTIVE'&& res.data.role === 'dokter'){
                store.set('loggedIn', true);
                this.props.history.push(res.data.userId)
            }
        })
        .catch( err => console.log(err) )
    }

    render() {
        console.log('ini login',(loggedIn()) )
        console.log('ini id', this.state.userid)
        const error = this.state.error
        console.log('ini props', this.props)
        console.log(store.get('loggedIn'))
        // console.log(store.get('userID'))
        if (loggedIn()) {
            return <Redirect to={`/dokter/${this.state.userid}`} />
        }

        return(
            <div>
                <PasungNavbarTwo />
                <Container>
                    <Row>
                    <div className='ContainerSec'>
                            <div className ='Card'>
                                <h1 className='TitleRegister'>Login</h1>
                                <div>
                                                <div className="login-wrapper">
                                                    <div className="login-page">
                                                        <form className="login-form">
                                                            <h1 className="signin-text">Sign In</h1>
                                                        
                                                            <input className="inputField" name="email" type="text" placeholder="email user" onChange={this.handleChange} value={this.state.email} required />
                                                            <br/>
                                                            <input className="inputField" name="password" type="password" placeholder="password" onChange={this.handleChange} value={this.state.password} required />
                                                            <br/>
                                                           
                                                            <button className="login-button" onClick={ (e) => this.handleSubmit(e)}>Masuk</button>
                                                            
                                        </form>
                                     </div>
                                 </div>
                                               
                            </div>

                        </div>
                     </div>
                            <div className='ContainerSec'>
                                <div className ='CardLoginSosial'>
                                 
                                    <p className='SosialTag'>Atau Masuk Dengan:</p>
                                        <img src={IconFb} alt="IconFb"className='IconFb' ></img>
                                        <img src={IconGmail} alt="IconGmail"className='IconGmail' ></img>    
                                    </div>
                               
                            </div>
                    </Row>
                </Container>
            </div>
        )

    }
}
export default PasungLoginDokter;
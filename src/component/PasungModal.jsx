import React, {Component} from 'react';
import {
   Row,Col,
    Modal,ModalBody,ModalHeader,Button} from 'reactstrap';
import'../assets/style/PasungStyle.scss';
import {Link} from 'react-router-dom';




class PasungModal extends Component{
    constructor(props) {
        super(props);
        this.state = {

            modal: false,

        }
        this.toggle = this.toggle.bind(this);

    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }
    render(){
        return(
            <div>
                <Row>
                        <Col>
                        <div>
                            <p className='register' onClick={this.toggle}>Register</p>
                            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                            <ModalHeader toggle={this.toggle}>Hallo, Siapa Anda ?</ModalHeader>
                            <ModalBody>
                            <div className='PasungBodyModal'>
                             <Link to='/registerpelapor' ><Button className='ModalBtnOne' color="danger">Pelapor</Button></Link>
                             <Link to='/registerdokter' ><Button className='ModalBtnOne' color="danger">Dokter</Button></Link>
                            
                            </div>
                            <div className='PasungBodyModalText'>
                            <p>Sudah Punya akun ? <a href='/login' >Login</a></p>
                            </div>
                            </ModalBody>
                            </Modal>
                        </div> 
                        </Col>
                    </Row>
                </div>
        )
    }
}

export default PasungModal ;
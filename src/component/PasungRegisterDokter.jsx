import React,{Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container,Row,Col} from 'reactstrap';
import { Button, Form, FormGroup, Label, Input, } from 'reactstrap';
import '../assets/style/PasungStyle.scss';
import PasungNavbarTwo from './PasungNavbarTwo';
import axios from 'axios';


class PasungRegisterPelapor extends Component{
constructor(prop){
    super(prop);
    this.state={
        email:'',
        fullName:'',
        password:'',
        phoneNumber:''
     

    }
}
// methot post
handleemailChange = event =>{this.setState({email:event.target.value})};
handlefullNameChange = event =>{this.setState({fullName:event.target.value})};
handlepasswordChange = event =>{this.setState({password:event.target.value})};
handlephoneNumberChange = event =>{this.setState({phoneNumber:event.target.value})};
handleSudmit= event =>{
    event.preventDefault();
    //link axios post
    axios.post(`https://stoppasung.herokuapp.com/user/registration/dokter`,
    {
        email:this.state.email,
        fullName:this.state.fullName,
        password:this.state.password,
        phoneNumber:this.state.phoneNumber
    }
)
.then(respon =>{
    console.log(respon);
    console.log(respon.data);
    // mothot untuk berhasil tapi tidak reload
    this.props.history.push('/aktivasidokter')

})

}

    render(){
        console.log("tes",this.state.fullName);
        return(
            <div>
                  <PasungNavbarTwo />
               <Container>
                    <Row>

                        <Col>
                        <div className='ContainerSec'>
                            <div className ='Card'>
                                <h1 className='TitleRegister'>Buat Akun Baru</h1>
                                    <Form onSubmit={this.handleSudmit}>
                                        <FormGroup>
                                        <Input type="text" name={this.state.fullName} onChange={this.handlefullNameChange} id="fullName" placeholder="Nama Lengkap Anda" />
                                        </FormGroup>
                                        <FormGroup>
                                        <Input type="email" name={this.state.email} onChange={this.handleemailChange} id="email" placeholder="Alamat Email" />
                                        </FormGroup>
                                        <FormGroup>
                                        <Input type="text" name={this.state.phoneNumber} onChange={this.handlephoneNumberChange} id="phoneNumber" placeholder="Nomor Telepon" />
                                        </FormGroup>
                                        <FormGroup>
                                        <Input type="Password" name={this.state.password} onChange={this.handlepasswordChange} id="password" placeholder="Password" />
                                        
                                        </FormGroup>
                                        <FormGroup check>
                                        <Label check>
                                        <Input type="checkbox"/>{' '}
                                         Dengan ini saya menyetujui <a href='#'>Syarat dan ketentuan </a>
                                        </Label>
                                        </FormGroup>
                                       <Button type='Sudmit' className='BtnDaftar'>Daftar</Button>
                                    </Form>
                            </div>
                            </div>
                        </Col>
                       
                            
                        
                    </Row>
                    </Container>
            </div>
        )
    }
}

export default PasungRegisterPelapor;
import React, {Component} from 'react';
import {Container,Row} from 'reactstrap';
import '../assets/style/PasungStyle.scss';
import HomeInfografis from '../assets/Img/HomeInfoGrafis.png'


class PasungBannerInfoGrafis extends Component {
    render(){
        return(
            <div>
                <Container>
                    <Row>
                        <div className='BackgroundInfografis'>
                       
                                <div className='ImgInfografis'>
                                    <img className='HomeInfoGrafis' src={HomeInfografis} alt='Infografis'></img>
                                </div>
                          
                     
                                <div className='TextInfoGrafis'>
                                <div className='TextInfo'>
                                    <p className='TextLineOneInfo'>Sekitar 4.478 orang mengalami pemasungan di tahun 2018</p>
                                    <p className='TextLineTwoInfo'>Satu orang yang berkeinginan untuk mengurangi pemasungan dapat memengaruhi banyak orang untuk melakukan hal yang sama.</p>
                                    </div>
                                </div>
                         
                        </div>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default PasungBannerInfoGrafis;
import React, {Component} from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    Nav,
    NavItem,
    NavLink,
    } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Link} from 'react-router-dom';
import '../assets/style/PasungStyle.scss';
import logo from '../assets/Icon/logo.png'

class PasungNavbarTwo extends Component{
    constructor(props) {
      super(props);
  
      this.toggle = this.toggle.bind(this);
      this.state = {
        isOpen: false
      };
    }
    toggle() {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
  render() {
    return (
      <div>
        <div className='NavTwo'>
        <Navbar >
        <Link to='/'><img className='logo2' src={logo} alt='Pasung.com'></img></Link>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
            <NavItem>
            <NavLink ><Link  className='NavLink' to='/login'>Login</Link></NavLink>
            </NavItem>
              <NavItem>
                <NavLink ><Link className='NavLink'  to='/register'>Register</Link></NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
        </div>
      </div>
    );
  }
}

export default PasungNavbarTwo;
import React from 'react';
import { Container, Col,Row } from 'reactstrap';
import FooterLogo from '../assets/Img/Footer/FooterIconLogo.svg';
import FooterIconInstagram from '../assets/Img/Footer/FooterIconInstagram.svg';
import FooterIconFB from '../assets/Img/Footer/FooterIconFb.svg';
import FooterIconTwitter from '../assets/Img/Footer/FooterIconTwitter.svg';
import '../assets/style/PasungStyle.scss'


class PasungFooter extends React.Component {
    render () {
        return (
        <div>
              <div className='FooterBackground'>
                    <Container>
                        <Row>
                            <Col md='6'>
                                <img src={FooterLogo} className='FooterIconLogo' alt='LogoPasung'/>
                            </Col>
                            <Col md='6' >
                                <div className='FooterBoxOne'>
                                <p className='TagLineFooterSatu'> Temukan kami di:</p>
                                <img src={FooterIconInstagram} className='FooterIconSatu' alt='instagram'/>
                                <img src={FooterIconFB} className='FooterIconSatu' alt='facebook'/>
                                <img src={FooterIconTwitter} className='FooterIconSatu' alt='twitter'/>
                                </div>
                            </Col> 
                        </Row>
                    </Container>
             </div>
                <Container className='FooterBoxTwo'>
                        <div>
                        <div className='FooterTagLineSatu'>
                            <p><a className='FooterTagLineSatuB' href='#'>StopPasung.com</a> adalah website untuk melaporkan kasus pemasungan yang terjadi di sekitar Anda.</p>
                        </div>


                        <div className='FooterTagLineDua'>
                            <p className='FooterTagLineDuaB'> Jl. BSD no 700 Tangerang, Banten </p>
                        </div>
                        </div>


                        <div className='FooterTagLineTiga'>
                            <p className='FooterTagLineTigaB'> UTAMA </p>
                            <p><a className='FooterTagLineTigaC' href='#'> Lapor Pemasungan </a></p>
                        </div>

                        
                        <div className='FooterTagLineEmpat'>
                            <p className='FooterTagLineEmpatB'> PELAJARI </p>
                            <p><a className='FooterTagLineEmpatC' href='#'> Tentang Kami </a></p>
                            <p><a className='FooterTagLineEmpatD' href='#'> Syarat dan Ketentuan </a></p>
                        </div>


                        <div className='d-md-none'>
                            <p className='FooterTagLineEmpatLima'> Jl. BSD no 700 Tangerang, Banten </p>
                        </div>
  
                </Container>
                
                <Container className='FooterLineBox'>
                    <div className='FooterLine'></div>
                    <p className='FooterLineSimbol'>Copyright © 2019 Pasung.com</p>
                </Container>
                </div>    
            
        );
    }
}


export default PasungFooter;
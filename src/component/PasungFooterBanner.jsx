import React from 'react';
import { Container, Row } from 'reactstrap';
import FooterKementrian from '../assets/Img/Footer/FooterKementrian.png';
import FooterBpjs from '../assets/Img/Footer/FooterBpjs.svg';

class PasungFooterBanner extends React.Component {
    render () {
        return (
            <div >
                <div>
                    <Container>
                   
                        <Row>
                        <div className='FooterBannerTwoBox'>
                        <p className='FooterBannerTagOne'>Kami Telah Bekerjasama Dengan:</p>
                        <img src={FooterBpjs} className='FooterBpjs' alt='bpjs'/>
                        <img src={FooterKementrian } className='FooterKementrian' alt='kemenkes'/>
                        </div>
                        </Row>
                      
                    </Container>
                </div>
            </div>

        );
    }
}

export default PasungFooterBanner;
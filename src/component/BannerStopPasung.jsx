import React, {Component} from 'react';
import '../assets/style/PasungStyle.scss';
import {Row,Col} from 'reactstrap';
import IconHomeMonitor from '../assets/Icon/IconHomeMonitor.svg';
import IconHomeDeatline from '../assets/Icon/IconHomeDeatline.svg';
import IconHomeDoctor from '../assets/Icon/IconHomeDoctor.svg';




class BannerStopPasung extends Component {
    render(){
        return(
            <div>
                <div className='BackgroundImg'>
                    <Row>
                        <Col>
                    <div className='BackText'>
                    <h2 className='TagLine'>Mengapa Lapor Lewat StopPasung ?</h2>
                    </div>
                    </Col>
                    </Row>

              
                <Row >
                <div className='BackgorundBan'>   
                                    <div className ='CardBanner' >
                                        <img className='IconCard' src={IconHomeDeatline}></img>
                                        <h3 className='title'>Efisien</h3>
                                        <p className='Deskripsi'>Membuat laporan sangat mudah dan cepat. Hanya dengan mengirimkan data pasien dan tunggu balasan dari kami.</p>
                                    </div>
                                    <div className ='CardBanner' >
                                        <img className='IconCard' src={IconHomeMonitor} alt="Card image cap"></img>
                                        <h3 className='title'>Di Pantau</h3>
                                        <p className='Deskripsi'>Anda akan mendapat pemberitahuan terbaru tentang perkembangan medis pasien yang anda laporkan.</p>
                                    </div>
                                    <div className ='CardBanner' >
                                        <img className='IconCard' src={IconHomeDoctor} alt="Card image cap"></img>
                                        <h3 className='title'>Terjamin</h3>
                                        <p className='Deskripsi'>Para Dokter yang menjadi mitra kami telah diseleksi. Anda tidak perlu khawatir tentang kemampuan  medis Dokter.</p>
                                    </div>
                                    </div>     
                 </Row>
                 </div>
            </div>
        )
    }
}

export default BannerStopPasung;
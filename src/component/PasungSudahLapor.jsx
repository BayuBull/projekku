import React from 'react';
import {Button} from 'reactstrap';
import {Row} from 'reactstrap';
import {Link} from 'react-router-dom';
import PasungNavbarTwo from './PasungNavbarTwo';

class SudahLapor extends React.Component {
    render () {
        return (
            <div>
            <PasungNavbarTwo />
            <Row>
            <div className='BgSudahLapor'>
                
                    <h1 className='TextSatuSudahLapor'>Terima Kasih!</h1>
                    <p className='TextDuaSudahLapor'>Selanjutnya tim Pasung.com akan melakukan validasi data dan melakukan kunjungan ke pasien.</p>
                    <p className='TextTigaSudahLapor'> Update informasi akan kami kirimkan ke email Anda.</p>
                

                <div className='BotSudahLapor'>
                <Link to='/'><Button className='BtnSudahLapor'>Kembali Ke Halaman Utama</Button></Link>
                </div>
            </div>
            </Row>
            </div>
            

        );
    }
}

export default SudahLapor;
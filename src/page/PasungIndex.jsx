import React, {Component} from 'react';
import PasungHeader from '../component/PasungHeader';
import BannerStopPasung from '../component/BannerStopPasung';
import PasungFooter from '../component/Pasung Footer';
import PasungBannerTwo from '../component/PasungBannerTwo';
import PasungFooterBanner from '../component/PasungFooterBanner'
import PasungBannerInfoGrafis from '../component/PasungBannerInfoGrafis';
import PasungBannerTahap from '../component/PasungBannerTahap';
import { Button } from 'reactstrap';
import '../assets/style/PasungStyle.scss';
import PasungNavbar from '../component/PasungNavbar';
import {Row} from 'reactstrap';
import {Link,Redirect} from 'react-router-dom';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    Nav,
    NavItem,
    NavLink,
    } from 'reactstrap';
    import logo from '../assets/Icon/logo.png';
import PasungModal from '../component/PasungModal';
import loggedIn from '../helper/is_logged_in';
import store from 'store';
import PasungModalLogin from '../component/PasungModalLogin';





class PasungIndex extends Component{

    //nav
    constructor(props) {
        super(props);
        this.handleLogout = this.handleLogout.bind(this);
  
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false
        };
      }
      toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }
      handleLogout = (e) => {
        store.remove('loggedIn');
        this.props.history.push('/login') 
  
    }

    // nav
    
    render(){
        console.log('ini login', loggedIn())

        let sudahlogin;
        if(loggedIn()) {
            
          sudahlogin =  <NavLink > <button onClick={this.handleLogout}>Logout</button></NavLink>
        } else {
          sudahlogin =  <NavLink ><PasungModalLogin /></NavLink>
        }
    
        let registerin;
        if(loggedIn()) {
          registerin = <NavLink><Link to='/profiledokter'>Profile</Link></NavLink>
        } else {
          registerin = <NavLink ><PasungModal /></NavLink>
        }
        console.log('nama',this.props)
        return(
            <div>
              <div className='BackgroundHeader'>
                                        <div>
                                                <div className='Nav'>
                                                <Navbar dark expand="md">
                                                <Link to='/'><img className='logoone' src={logo} alt='Pasung.com'></img></Link>
                                                <NavbarToggler onClick={this.toggle} />
                                                <Collapse isOpen={this.state.isOpen} navbar>
                                                    <Nav className="ml-auto" navbar>
                                                    <NavItem>
                                                        {sudahlogin}
                                                    </NavItem>
                                                    <NavItem>
                                                        {registerin}
                                                    </NavItem>
                                                    </Nav>
                                                </Collapse>
                                                </Navbar>
                                                </div>
                                            </div>
                    <div className='ContainerHeader'>
                    <div className='PasungTaglineHome'>
                    <p>Laporkan Kasus Pemasungan di Sekitar Anda</p>
                    </div>
                    <div className="ContainerBotHome">
                    <Row>
                    <Link  className='BtnLapor' to='/lapor'><Button  className='Lapor'>Lapor Sekarang</Button></Link>
                    <Link  className='BtnDocter' to='/aktivasidokter'><Button outline color className='Docter'>Saya Dokter</Button></Link>
                    </Row>
                    </div>
                    </div>
                    
                </div>
            <PasungBannerTahap />
            <BannerStopPasung />
            <PasungBannerInfoGrafis />
            <PasungBannerTwo />
            <PasungFooterBanner />
            <PasungFooter />
            </div>
        )
    }
}
export default PasungIndex;
